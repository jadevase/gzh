
<?php
header("Content-Type:application/json;charset=UTF-8");
  // create short variable names
  // 通过post方法传递检索字段和检索数据
 // echo $_POST;
  $searchtype=$_POST['searchtype'];
  $searchterm=trim($_POST['searchterm']);

  if (!$searchtype || !$searchterm) {
     echo '请给出检索字段和检索内容.';
     exit;
  }

  if (!get_magic_quotes_gpc()){
  //在特殊符号前添加反斜杠
    $searchtype = addslashes($searchtype);
    $searchterm = addslashes($searchterm);
  }

  @ $db = new mysqli('localhost', 'xcx', 'magic123', 'yanzhi');

  if (mysqli_connect_errno()) {
     echo 'Error: Could not connect to database.  Please try again later.';
     exit;
  }
//  $query = "select * from anchors where ".$searchtype." like '%".$searchterm."%'";

  $query = "select * from anchors";
  $result = $db->query($query);

  $num_results = $result->num_rows;

//  echo "<p>Number of Anchors found: ".$num_results."</p>";
  $json="";
  $data=array();
  class anchors
  {
      public $anchorid;
      public $wxname;
      public $wxid;
      public $city;
      public $gender;
      public $age;
      public $douyinid;
      public $quotation;
      public $image_normal_url;
      public $image_original_url;
      public $image_ipad_url;
      
  }
  for ($i=0; $i <$num_results; $i++) {
     $row = $result->fetch_assoc();
     $anchor=new anchors();
     $anchor->anchorid = $row["anchorid"];
     $anchor->wxid = $row["wxid"];
     $anchor->city = $row["city"];
	 $anchor->wxname = $row["wxname"];
	 $anchor->gender = $row["gender"];
	 $anchor->age = $row["age"];
	 $anchor->douyinid = $row["douyinid"];
	 $anchor->quotation = $row["quotation"];
	 $anchor->image_normal_url = $row["image_normal_url"];
	 $anchor->image_original_url = $row["image_original_url"];
	 $anchor->image_ipad_url = $row["image_ipad_url"];
	 $data[]=$anchor;
	 /*
	 echo "<p><strong>".($i+1).". ID: ";
     echo htmlspecialchars(stripslashes($row['anchorid']));
     echo "</strong><br />微信昵称: ";
     echo stripslashes($row['wxname']);
     echo "</strong><br />微信号码: ";
     echo stripslashes($row['wxid']);
     echo "</strong><br />城市：";
     echo stripslashes($row['city']);
     echo "</strong><br />语录 ";
     echo stripslashes($row['quotation']);
     echo "</p>";
*/
	 }
  $json=json_encode($data);
  echo $json;
  $result->free();
  $db->close();

?>
