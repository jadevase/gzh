<html>
<head>
  <title>Anchors Search Results</title>
</head>
<body>
<h1>Anchors Search Results</h1>
<?php
  // create short variable names
  $searchtype=$_POST['searchtype'];
  $searchterm=trim($_POST['searchterm']);

  if (!$searchtype || !$searchterm) {
     echo 'You have not entered search details.  Please go back and try again.';
     exit;
  }

  if (!get_magic_quotes_gpc()){
  //在特殊符号前添加反斜杠
    $searchtype = addslashes($searchtype);
    $searchterm = addslashes($searchterm);
  }

  @ $db = new mysqli('localhost', 'xcx', 'magic123', 'yanzhi');

  if (mysqli_connect_errno()) {
     echo 'Error: Could not connect to database.  Please try again later.';
     exit;
  }

  $query = "select * from anchors where ".$searchtype." like '%".$searchterm."%'";
  $result = $db->query($query);

  $num_results = $result->num_rows;

  echo "<p>Number of Anchors found: ".$num_results."</p>";

  for ($i=0; $i <$num_results; $i++) {
     $row = $result->fetch_assoc();
     echo "<p><strong>".($i+1).". ID: ";
     echo htmlspecialchars(stripslashes($row['anchorid']));
     echo "</strong><br />微信昵称: ";
     echo stripslashes($row['wxname']);
     echo "</strong><br />微信号码: ";
     echo stripslashes($row['wxid']);
     echo "</strong><br />城市：";
     echo stripslashes($row['city']);
     echo "</strong><br />语录 ";
     echo stripslashes($row['quotation']);
     echo "</p>";
  }

  $result->free();
  $db->close();

?>
</body>
</html>